import React from 'react';
import renderer from 'react-test-renderer';

import AssemblLogin from '../../../../js/app/components/login/assemblLogin';

describe('AssemblLogin component', () => {
  it('should match the snapshot', () => {
    const component = renderer.create(<AssemblLogin />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});