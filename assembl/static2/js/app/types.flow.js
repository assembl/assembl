// @flow
import { ExportDataTypes, LocalesObject, PublicationStates } from './constants';

export type AvailableLocales = $Keys<typeof LocalesObject>;
export type ExportDataTypesType = $Values<typeof ExportDataTypes>;
export type PublicationStateTypes = $Keys<typeof PublicationStates>;

export type RouterParams = {
  /** Fiction identifier */
  fictionId?: string,

  /** Fiction phase identifier */
  phase: string,

  /** Question identifier */
  questionId?: string,

  /** Question index */
  questionIndex?: string,

  /** Discussion slug */
  slug: string,

  /** Fiction theme identifier */
  themeId?: string
};