// @flow
/* eslint-disable jsx-a11y/media-has-caption */

import * as React from 'react';

type Props = {
  embedUrl: string
};

const MediactiveEmbed = ({ embedUrl }: Props) => (
  <div className="embed-responsive embed-responsive-16by9">
    <video width="1200" height="600" controls>
      <source src={embedUrl} type="video/mp4" />
      <p>Your browser does not support the video tag.</p>
      <p>
        <a href={embedUrl}>video.mp4 download</a>
      </p>
    </video>
  </div>
);

export default MediactiveEmbed;