// @flow
import * as React from 'react';

type Props = {
  embedUrl: string,
  title: string,
  id: string
};

const DefaultEmbed = ({ embedUrl, title, id }: Props) => (
  <div className="embed-responsive embed-responsive-16by9">
    <iframe title={title} id={id} type="text/html" width="640" eight="360" src={embedUrl} frameBorder="0" allow="fullscreen" />
  </div>
);

export default DefaultEmbed;