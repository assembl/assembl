/* eslint-disable max-len */
import React from 'react';

class ElFlag extends React.Component {
  render() {
    return (
      <svg
        width="20px"
        height="20px"
        x="0px"
        y="0px"
        viewBox="0 0 512 512"
        style={{ enableBackground: 'new 0 0 512 512' }}
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
      >
        <path
          style={{ fill: 'url(#pattern0)' }}
          d="M101 202C156.781 202 202 156.781 202 101C202 45.2192 156.781 0 101 0C45.2192 0 0 45.2192 0 101C0 156.781 45.2192 202 101 202Z"
        />
        <defs>
          <pattern
            id="pattern0"
            patternContentUnits="objectBoundingBox"
            width="1"
            height="1"
          >
            <use
              xmlnsXlink="#image0"
              transform="translate(-0.00742574) scale(0.00588235)"
            />
          </pattern>
          <image
            id="image0"
            width="255"
            height="170"
            xmlnsXlink="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP8AAACqCAMAAABVlWm8AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAANlBMVEUNXq9DgsH///+vyuUcaLQpcLiUuNxtns/A1eqNs9nK3O15ptPk7faCrNZflMpXj8erx+Pc6PPi4J5ZAAAAAWJLR0QCZgt8ZAAAAAd0SU1FB+EICgIeHmPLH/AAAAD+SURBVHja7dhBDoIwEIbRAlIRFfT+l/UAjUkT06RO37efxZvln1JN01y0pIHi5+fn5+fn5+fn5+fn5+fn5//SZe28xv48dx4/Pz8/Pz8/Pz8/Pz8/Pz9/eP9U07U83KoOb3vnpbbvvXc/bfLz8/Pz8/Pz8/Pz8/Pz8/NH9y81baXsUXX4PDrP/svPz8/Pz8/Pz8/Pz8/Pz8/P/4P/zJ3X2B8kfn5+fn5+fn5+fn5+fn5+/j+uaiV6lf53DlGax46fn5+fn5+fn5+fn5+fn58/vP8YuyRJkiRJkqTY7WNn/+Xn5+fn5+fn5+fn5+fn5+cP7l/HzgAqSZIkSZIUuw9pWsllJYdqMQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNy0wOC0xMFQwMjozMDozMCswMDowMOaWY+AAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTctMDgtMTBUMDI6MzA6MzArMDA6MDCXy9tcAAAAAElFTkSuQmCC"
          />
        </defs>
      </svg>
    );
  }
}

export default ElFlag;