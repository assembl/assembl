/* eslint-disable max-len */
import React from 'react';

class IdFlag extends React.Component {
  render() {
    return (
      <svg
        width="20px"
        height="20px"
        x="0px"
        y="0px"
        viewBox="0 0 512 512"
        style={{ enableBackground: 'new 0 0 512 512' }}
        xmlnsXlink="http://www.w3.org/1999/xlink"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          style={{ fill: 'url(#pattern0)' }}
          d="M101 202C156.781 202 202 156.781 202 101C202 45.2192 156.781 0 101 0C45.2192 0 0 45.2192 0 101C0 156.781 45.2192 202 101 202Z"
        />
        <defs>
          <pattern
            id="pattern0"
            patternContentUnits="objectBoundingBox"
            width="1"
            height="1"
          >
            <use
              xlinkHref="#image0"
              transform="translate(-0.170792) scale(0.00588235)"
            />
          </pattern>
          <image
            id="image0"
            width="255"
            height="170"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP8AAACqAQMAAABYhQvNAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABlBMVEX/AAD///9BHTQRAAAAAWJLR0QB/wIt3gAAAAd0SU1FB+IJBgQ0OCsCzjEAAAAoSURBVFjD7coxEQAACASg7x9abeCuBzMJAAB817sSBEEQBEEQhOthAAmmlaLNs4KMAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTA5LTA2VDA0OjUyOjU2KzAwOjAw0MOgQQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0wOS0wNlQwNDo1Mjo1NiswMDowMKGeGP0AAAAASUVORK5CYII="
          />
        </defs>
      </svg>
    );
  }
}

export default IdFlag;