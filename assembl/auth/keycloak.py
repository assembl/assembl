"""This module implements login and credentials obtention from keycloak

This is a plain OIDC implementation based on SocialAuth
"""

from social_core.backends.open_id_connect import OpenIdConnectAuth

from assembl.lib import config
from assembl.lib.logging import getLogger

log = getLogger('assembl')


class KeycloakAuth(OpenIdConnectAuth):
    """This is just a helper to facilitate connection through Keycloak

    Set keycloak_oidc_endpoint to the URL of the realm and connection will happen
    automagically. This is done through the keycloak_oidc_endpoint config entry

    In case of a private SSO platform you will also need to set
    keycloak_oidc_key and keycloak_oidc_secret in the config.

    For the moment the following restrictions apply:

    - the userinfo must be unsigned
    - no support for permissions
    """
    name = 'assembl-sso'
    USERNAME_KEY = 'username'
    IGNORE_DEFAULT_SCOPE = False
    DEFAULT_SCOPE = ['openid', 'profile', 'email']

    def __init__(self, *args, **kwargs):
        super(KeycloakAuth, self).__init__(*args, **kwargs)
        self.OIDC_ENDPOINT = config.get('keycloak_oidc_endpoint', None)

    def get_key_and_secret(self):
        key = config.get('keycloak_oidc_key', None)
        sec = config.get('keycloak_oidc_secret', None)
        return key, sec

    def request_access_token(self, *args, **kwargs):
        kwargs['data']['response_type'] = 'id_token token'
        return super(KeycloakAuth, self).request_access_token(
            *args, **kwargs
        )

    def auth_extra_arguments(self):
        return {'scope': 'openid'}

    def auth_complete_params(self, state=None):
        params = super(KeycloakAuth, self).auth_complete_params(state)
        params['grant_type'] = 'authorization_code'
        return params
