import requests
from urlparse import urljoin
from assembl.lib.logging import getLogger

log = getLogger()


class Lockout(object):

    EVENT_URL = '/v1/event'
    PRISON_URL = '/v1/prisoner/{namespace}/{id}'
    TIMEOUT = 60

    def __init__(self, endpoint, uid, entity_attr='id'):
        self.url_base = endpoint
        if not uid:
            self.unique_id = 'assembl-assembl'
        else:
            self.unique_id = 'assembl-' + uid
        self.entity_attr = entity_attr

    @classmethod
    def from_config(cls, conf):
        url = conf.get('lockout.endpoint', None)
        uid = conf.get('lockout.unique_id', None)
        return Lockout(url, uid)

    def get_extra_data(self, user):
        """Override for own logic on the types of exta data to pass"""
        return {}

    def get_entity_id(self, user):
            return getattr(user, self.entity_attr)

    def send_event(self, user):
        url = urljoin(self.url_base, Lockout.EVENT_URL)
        entity_id = self.get_entity_id(user)
        extra_data = self.get_extra_data(user)
        try:
            if not isinstance(entity_id, basestring):
                entity_id = str(entity_id)
            data = {
                'entity-id': entity_id,
                'source': self.unique_id,
                'entity-details': extra_data
            }
            res = requests.post(url, json=data, timeout=Lockout.TIMEOUT)
            if res.status_code > 400 and res.status_code < 500:
                log.error('[lockout] Failed to push event to lockout service to url "%s" with data: \n%s' % (
                    url, data)
                )
                return False
            if res.status_code > 500:
                log.error('[lockout] Lockout service failed to capture event for user %s' % entity_id)
                return False
            return True
        except Exception as e:
            log.error('[lockout] An error occured in sending event for user %s to lockout with error %s' % (
                user, e))
        finally:
            return False

    def get_jail_url(self, user):
        entity_id = self.get_entity_id(user)
        prison_url = Lockout.PRISON_URL.format(namespace=self.unique_id, id=entity_id)
        url = urljoin(self.url_base, prison_url)
        return url

    def is_jailed(self, user):
        url = self.get_jail_url(user)
        try:
            res = requests.get(url, timeout=Lockout.TIMEOUT)
            if res.status_code > 200 and res.status_code < 300:
                return True
            elif res.status_code > 400 and res.status_code < 500:
                return False
            else:
                # Error condition, assume NOT in jail
                log.error('[lockout] Lockout service failed to send jail information for user %s' % user)
                return False
        except Exception as e:
            log.error('[lockout] An error occured in sending event of user %s to lockout with error %s' % (
                user, e))
            return False

    def bail_user(self, user):
        url = self.get_jail_url(user)
        try:
            res = requests.delete(url, timeout=Lockout.TIMEOUT)
            if res.status_code > 200 and res.status_code < 300:
                return True
            elif res.status_code > 400 and res.status_code < 500:
                return False
            else:
                log.error('[lockout] Lockout service failed to bail user %s' % user)
                return False
        except Exception as e:
            log.error('[lockout] An error occured in bailing user %s with error %s' % (
                user, e))
            return False

    def bail_if_jailed(self, user):
        if self.is_jailed(user):
            return self.bail_user(user)
