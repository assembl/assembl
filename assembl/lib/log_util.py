'''Logging utility functions
'''

from inspect import (
    getmodulename,
    getinnerframes,
    isbuiltin,
    ismodule,
    isclass,
    ismethod,
    isfunction,
    isgeneratorfunction,
    isgenerator,
)
from os.path import exists, normpath, relpath, dirname
from sys import exc_info, modules
from traceback import extract_tb

import assembl


def full_qual_name(obj):
    '''Get the fully qualified name of the object $obj.
    '''
    mod = getattr(obj, '__module__', None)
    cls = getattr(obj, '__class__', '')

    cls = cls and cls.__name__


    if 'builtin' in cls:
        mod = cls = None
    elif cls in ('type', 'function', 'method'):
        cls = None

    name = getattr(obj, '__name__', None)
    fqn = '.'.join(n for n in (mod, cls, name) if n)

    return fqn


def format_vars(dico):
    '''Process namespace $dico for logging.

        - remove builtins
        - format values
        - trim the paths
        - remove privates
    '''
    root = dirname(assembl.__file__)

    def relative(path):
        try:
            if exists(normpath(path)):
                return '*/'+relpath(path, root)
        except: pass

    def isnamed(obj):
        return (isfunction(obj)
            or ismethod(obj)
            or isclass(obj)
            or ismodule(obj)
            or isgenerator(obj)
            or isgeneratorfunction(obj)
        )

    def convert(val):
        if isnamed(val):
            return full_qual_name(val)

        rel = relative(val)
        if rel:
            return rel

        return str(val)

    items = {
        k : convert(v)
        for k, v in dico.items()
        if (
            not isbuiltin(v)
            and k!='__builtins__'
            and not k.startswith('_')
        )
    }

    return items


def exc_context():
    '''Get last exception's context

        Must be called in an except block.
    '''
    typ, val, tbk = exc_info()
    org = extract_tb(tbk)[-1]
    fil, num, fct, src = org
    frames = getinnerframes(tbk)
    frame = frames[-1][0]
    module = getmodulename(fil)

    context = dict(
        exc_type = typ,                           # exc class
        exc_msg = val.message,                    # exc message
        module = module,                          # exc module's name
        function = fct,                           # exc function's name
        line_num = num,                           # exc line number
        f_locals = format_vars(frame.f_locals),   # exc frame's locals
        f_globals = format_vars(frame.f_globals), # exc frame's globals
    )

    return context
