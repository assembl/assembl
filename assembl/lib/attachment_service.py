import hashlib
from os import path
from tempfile import TemporaryFile

from .config import get


class AttachmentService(object):
    def __init__(self):
        pass

    def computeHash(self, dataf):
        hashobj = hashlib.new('SHA256')
        if hasattr(dataf, 'read'):
            pos = dataf.tell()
            for data in dataf:
                hashobj.update(data)
            dataf.seek(pos)
        else:
            with open(dataf, 'rb') as stream:
                for data in stream:
                    hashobj.update(data)
        return hashobj.hexdigest()

    @classmethod
    def get_service(cls):
        if not hasattr(cls, '_service'):
            service = get('attachment_service', 'hashfs')
            if service == 'hashfs':
                cls._service = HashFsAttachmentService()
            elif service == 's3':
                cls._service = AmazonAttachmentService()
            elif service == 's3compatible':
                cls._service = S3CompatibleAttachmentService()
            else:
                raise RuntimeError("No attachment service")
        return cls._service


class HashFsAttachmentService(AttachmentService):
    def __init__(self):
        from .hash_fs import get_hashfs
        self.hashfs = get_hashfs()

    def put_file(self, dataf):
        # dataf may be a file-like object or a file path
        address = self.hashfs.put(dataf)
        return address.id

    def get_file_path(self, fileHash):
        return self.hashfs.get(fileHash).abspath.encode('ascii')

    def get_file_stream(self, fileHash):
        return open(self.get_file_path(fileHash), 'rb')

    def get_file_url(self, fileHash):
        return b'/private_uploads' + self.get_file_path(fileHash)[len(self.hashfs.root):]

    def delete_file(self, fileHash):
        self.hashfs.delete(fileHash)

    def exists(self, fileHash):
        return path.exists(self.get_file_path(fileHash))


class AmazonAttachmentService(AttachmentService):
    def __init__(self):
        import boto3
        import botocore.client
        self.bucket_name = get('attachment_bucket', 's3_attachments')
        self.bucket_path_prefix = get('attachment_prefix', '')
        bucket_s3_endpoint = get('attachment_s3_endpoint', '')
        bucket_s3_access_key = get('attachment_s3_access_key', '')
        bucket_s3_secret_key = get('attachment_s3_secret_key', '')
        region = get('aws_region')
        # see https://boto3.amazonaws.com/v1/documentation/api/1.9.42/guide/s3.html
        addressing_style = get('attachment_s3_addressing_style', 'auto')
        botoconfig = botocore.client.Config(s3={
                'addressing_style': addressing_style
            })
        if bucket_s3_endpoint != '':
            self.s3 = boto3.resource('s3',
                    endpoint_url=bucket_s3_endpoint,
                    region_name=region,
                    aws_access_key_id=bucket_s3_access_key,
                    aws_secret_access_key=bucket_s3_secret_key,
                    config=botoconfig
                    )
        else:
            self.s3 = boto3.resource('s3', region)
        self.s3c = self.s3.meta.client
        self.bucket = self.s3.Bucket(self.bucket_name)

    def get_prefixed_key(self, key):
        return '{}{}'.format(self.bucket_path_prefix, key)

    def put_file(self, dataf, mimetype=None):
        key = self.computeHash(dataf)
        obj_path = self.get_prefixed_key(key)
        if not self.exists(key):
            if hasattr(dataf, 'read'):
                self.bucket.upload_fileobj(dataf, obj_path, {
                    'ContentType': mimetype or 'application/octet-stream'
                })
            else:
                self.bucket.upload_file(dataf, obj_path, {
                    'ContentType': mimetype or 'application/octet-stream'
                })
        return key

    def get_file_path(self, fileHash):
        return None

    def get_file_stream(self, fileHash):
        f = TemporaryFile()
        obj_path = self.get_prefixed_key(fileHash)
        self.bucket.download_fileobj(obj_path, f)
        f.seek(0)
        return f

    def get_file_url(self, fileHash):
        base_url = self.s3c.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': self.bucket_name,
                'Key': self.get_prefixed_key(fileHash)
            }
        )
        return b'/private_uploads/' + base_url.split('/', 3)[-1].encode('ascii')

    def delete_file(self, fileHash):
        obj_path = self.get_prefixed_key(fileHash)
        self.bucket.delete_objects(Delete={'Objects': [{'Key': obj_path}]})

    def exists(self, fileHash):
        obj_path = self.get_prefixed_key(fileHash)
        return bool([x for x in self.bucket.objects.filter(Prefix=obj_path) if x.key == obj_path])


class S3CompatibleAttachmentService(AmazonAttachmentService):
    """This is basically the AmazonAttachmentService driver with fixes for the
       quirky implementation details
    """
    def exists(self, fileHash):
        from botocore.exceptions import ClientError
        obj_path = self.get_prefixed_key(fileHash)
        try:
            return bool([x for x in self.bucket.objects.filter(Prefix=obj_path) if x.key == obj_path])
        except ClientError as ex:
            if ex.response['Error']['Code'] == 'NoSuchKey':
                # sloppy but existing s3 implementation
                return False
            else:
                raise
