'''Third party snippets handling
    Snippets must be put in {snippets_dir} by devpos
'''
# PHENIX-400
from os import listdir
from os.path import join, isdir

from bs4 import BeautifulSoup
from pyramid.settings import asbool

from assembl.lib.config import get
from assembl.lib.logging import getLogger


enabled = asbool(get('enable_third_party_cookies', False))
snippets_dir = get('snippets_dir')

log = getLogger('assembl')


def list_snippets():
    ''' Get snippets names from snippets dir.
    '''
    return [
        name
        for name in listdir(snippets_dir)
        if not name.startswith('.')
    ]


def read_snippet(name):
    ''' Get snippet content
    '''
    raw = open(join(snippets_dir, name)).read()
    # breaks jinja2 | safe
    # snippets must be ascii
    # content = unicode(raw.strip(), 'utf8')
    return raw.strip()


def check_snippet(name):
    ''' Check snippet content.
    '''
    try:
        snippet = read_snippet(name)
        BeautifulSoup(snippet)  # Validate that it is HTML content
        return True
    except Exception as e:
        print 'SNIPPET :', snippet
        log.error('Invalid snippets : %s', e)
        return False


def check_snippets():
    ''' Check snippets setup.

        should be called on startup.
    '''
    if enabled and snippets_dir:
        if not isdir(snippets_dir):
            log.error('Snippets dir missing %s', snippets_dir)
            return False

        return all(
            check_snippet(name)
            for name in list_snippets()
        )
    else:
        return True


# TODO: add cache if necessary.
def load_snippets():
    ''' Load snippets content in a dict.
    '''
    if enabled:
        return {
            name: read_snippet(name)
            for name in list_snippets()
        }
    else:
        return {}

