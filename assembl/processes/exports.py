from __future__ import print_function

import traceback
from celery.utils.log import get_logger
from pyramid.i18n import TranslationStringFactory
from pyramid_mailer.message import Attachment, Message
from urlparse import urljoin


from assembl.processes import celery
from assembl.lib.config import get
from assembl.lib.utils import waiting_get
from assembl.scripts import get_exported_file_by_func
from assembl.views.api2.discussion import (
    ExportTypes,
    extract_taxonomy_csv,
    multi_module_csv_export,
    multi_module_posts_csv_export,
    users_csv_export,
    phase_csv_export,
    survey_csv_export,
    thread_csv_export,
    multicolumn_csv_export,
    voters_csv_export,
    bright_mirror_csv_export,
    global_votes_csv_export,
    get_contribution_count,
    get_visit_count,
    get_visitors,
)

log = get_logger(__name__)
_ = TranslationStringFactory('assembl')


def generate_template(localizer, attachment=False, **kwargs):
    subject = localizer.translate(_(u"[{slug}] Data Export Request"))

    if not attachment:

        text_message = localizer.translate(_(u"""Hello, {name}, your request for exported data has been processed!

Please click on the link below to download the export:

<{export_url}>

All the best,
The Bluenove Team"""))

        html_message = localizer.translate(_(u"""<p>Hello, {name}, your request for exported data has been processed!</p>
    <p>Please click on the the link below to download the export:</p><p><a href="{export_url}">{export_url}</a></p>
    <p>All the best,<br />The Bluenove Team</p>"""))

    else:
        text_message = localizer.translate(_(u"""Hello, {name}, your request for exported data has been processed! Please refer to the attachment below.

All the best,
The Bluenove Team"""))

        html_message = localizer.translate(_(u"""<p>Hello, {name}, your request for exported data has been processed! Please refer to the attachment below.</p>
    <p>All the best,<br />The Bluenove Team</p>"""))

    return (subject.format(**kwargs), text_message.format(**kwargs), html_message.format(**kwargs))


def generate_url(filename):
    account_number = get('aws_client', None)
    region = get('aws_region', None)
    assert account_number, "No Account Number associated to export"
    assert region, "No region associated to export"
    base_url = "https://assembl-data-{account_number}.s3-{region}.amazonaws.com/".format(account_number=account_number, region=region)

    return urljoin(base_url, filename)


@celery.task
def complete_export(
        export_type,
        user_id,
        discussion_id,
        lang='fr',
        anon='false',
        social_columns=True,
        start=None,
        end=None,
        interval=None,
        publication_state=None):
    if not user_id:
        log.error("[Exports] No user_id was passed to task. Stopping...")
        raise Exception("user_id was not passed to task")
    else:
        from assembl.models import User, Discussion
        user = waiting_get(User, user_id)
        assert user, "No user with user_id: %s found to send exports to" % user_id
        email = user.get_preferred_email()
        log.info("[Exports] User with user_id: %s and email %s found!" % (user_id, email))

        discussion = waiting_get(Discussion, discussion_id)
        assert discussion, "No discussion with discussion_id: %s was found to genereate exports from" % discussion_id

        export_func = None
        try:
            t = ExportTypes(export_type)
        except Exception:
            t = ExportTypes.MULTI_MODULE

        if t == ExportTypes.TAXONOMY:
            export_func = extract_taxonomy_csv
        elif t == ExportTypes.MULTI_MODULE:
            export_func = multi_module_csv_export
        elif t == ExportTypes.MULTI_MODULE_POSTS:
            export_func = multi_module_posts_csv_export
        elif t == ExportTypes.USERS:
            export_func = users_csv_export
        elif t == ExportTypes.PHASE:
            export_func = phase_csv_export
        elif t == ExportTypes.SURVEY:
            export_func = survey_csv_export
        elif t == ExportTypes.THREAD:
            export_func = thread_csv_export
        elif t == ExportTypes.MULTI_COLUMN:
            export_func = multicolumn_csv_export
        elif t == ExportTypes.VOTES_USER_DATA:
            export_func = voters_csv_export
        elif t == ExportTypes.BRIGHT_MIRROR:
            export_func = bright_mirror_csv_export
        elif t == ExportTypes.GLOBAL_VOTES:
            export_func = global_votes_csv_export
        elif t == ExportTypes.CONTRIBUTIONS_COUNT:
            export_func = get_contribution_count
        elif t == ExportTypes.VISITS_COUNT:
            export_func = get_visit_count
        elif t == ExportTypes.LIST_VISITORS:
            export_func = get_visitors
        else:
            # The default export otherwise, for whatever reason
            export_func = multi_module_csv_export

        output = get_exported_file_by_func(
            export_func,
            discussion,
            lang=lang,
            anon=anon,
            social_columns=social_columns,
            start=start,
            end=end,
            interval=interval,
            publication_state=publication_state
        )

        has_attachment = isinstance(output, tuple)
        if not has_attachment:
            filename = output
            log.info("[Exports] An export type of %s was completed and uploaded with the filename: %s" % (t, filename))
        else:
            filename = output[1]

        # prepare to email off
        mailer = celery.mailer
        data = dict(
            slug=discussion.slug,
            name=user.name
        )
        if not has_attachment:
            data.update(export_url=generate_url(filename))
        localizer = celery._pyramid_localizer(lang)
        subject, text, html = generate_template(localizer, attachment=has_attachment, **data)
        message = Message(
            subject=subject,
            sender=get('assembl.admin_email'),
            recipients=["%s <%s>" % (user.name, email)],
            body=text,
            html=html
        )
        try:
            if has_attachment:
                file, filename, mime_type = output
                with open(file) as f:
                    attachment = Attachment(
                        filename=filename,
                        data=f,
                        content_type='%s; charset=\'utf-8\'' % mime_type,
                        disposition='attachment; filename=%s' % filename)
                    # TODO: Add limit check for attachment size
                    message.attach(attachment)
                    mailer.send_immediately(message)
                    log.info("[Exports] An export of type %s was sent to %s with the export as an attachment." % (t, email))
            else:
                mailer.send_immediately(message)
                log.info("[Exports] An export of type %s was sent to %s" % (t, email))
        except Exception:
            error = traceback.format_exc()
            log.error("[Exports] An export of type %s was NOT able to be sent to %s. Here is the exception: \n%s" % (t, email, error))
