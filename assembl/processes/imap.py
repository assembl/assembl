"""A celery process that reads messages from an IMAP source."""
from . import celery


@celery.task(ignore_result=True, shared=False)
def import_mails(mbox_id, only_new=True):
    # This task is redundant - find out why but ack anyway
    return  "ok"
