from pyramid.settings import asbool
import sys

from assembl.lib.config import get
from assembl.lib.logging import getLogger
from assembl.lib.log_util import exc_context

use_new_logger = asbool(get('use_new_logger', False))

def logging_tween_factory(handler, registry):
    """This defines a tween that will log queries."""
    def logging_tween(request):
        log = request.logger()
        log.info('request', method=request.method, path=request.path, getargs=request.GET)
        try:
            response = handler(request)
            log.info('response', status=response.status)
            return response
        except Exception as e:
            if not use_new_logger:
                request.logger().error("responseError", exc_info=sys.exc_info())
            else:
                ctx = exc_context()
                log.error("responseError",
                    exception = '{exc_type} : {exc_msg}'.format(**ctx),
                    source = '{module}.{function}:{line_num}'.format(**ctx),
                    f_locals = '{f_locals}'.format(**ctx),
                    f_globals = '{f_globals}'.format(**ctx),
                )
            raise e

    return logging_tween
