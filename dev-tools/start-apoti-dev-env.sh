#!/bin/bash
# this script creates a docker-compose.yml for development purposes
errout() {
    echo $@
    exit 1
}
which realpath > /dev/null || errout "You need some core unix tools, please run: brew install coreutils"
PATH_TO_ASSEMBL=$(realpath $1)
ASSEMBL_ENV_NAME=$2

which envsubst > /dev/null || errout "Please run: brew install gettext && brew link --force gettext"
if [ -z "$PATH_TO_ASSEMBL" ]; then
    errout "Usage: $0 PATH_TO_ASSEMBL ASSEMBL_ENV_NAME"
fi
if [ ! -d "$PATH_TO_ASSEMBL/.git" ]; then
    echo "Path does not exist: $PATH_TO_ASSEMBL"rg
    errout "Usage: $0 PATH_TO_ASSEMBL ASSEMBL_ENV_NAME"
fi

echo "We will use assembl from path [$PATH_TO_ASSEMBL]"


echo -n "Creating directory ~/dev-assembl/${ASSEMBL_ENV_NAME}..."
mkdir -p ~/dev-assembl/$ASSEMBL_ENV_NAME
echo "done"
echo "Creating docker-compose.yml file in env $ASSEMBL_ENV_NAME..."
export PATH_TO_ASSEMBL
cat $PATH_TO_ASSEMBL/dev-tools/docker-compose.yml | envsubst > ~/dev-assembl/${ASSEMBL_ENV_NAME}/docker-compose.yml

cat <<EOF
OK. You can now start assembl by running:

  cd ~/dev-assembl/${ASSEMBL_ENV_NAME}
  docker-compose up -d


For first-time run of this environment, please then run
the following commands

  docker-compose exec assembl bash

then from inside the container

  cd /git-repo/dev-tools
  bash bootstrap-container.sh
  supervisorctl restart waitress

and connect to assembl at

  http://localhost:6543


Have fun!
EOF
