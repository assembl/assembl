#!/bin/bash
# use only for development, first time use
ln -s /git-repo/development.ini /app/development.ini

source /venv/bin/activate
cd /git-repo/
python setup.py develop
cd /app/assembl/static/js
yarn
cd /app/assembl/static2
yarn
yarn build
cd /app/
python assembl/scripts/po2json.py
bower --allow-root install

cd /app/assembl/static/js
node_modules/.bin/gulp libs
node_modules/.bin/gulp browserify:prod
node_modules/.bin/gulp sass

cd /app
supervisorctl restart waitress
supervisorctl restart webpack